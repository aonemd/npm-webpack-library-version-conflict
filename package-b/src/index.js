const $ = require('jquery');

export default class PackageB {
  constructor() {}

  render() {
    console.log('PACKAGE B has jquery version: ', $.fn.jquery);
    $('#change-me-b').text(`Package B changed this text by jQuery version ${$.fn.jquery}`);
  }
}

// new PackageB().render();
