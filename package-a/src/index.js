import $ from "jquery";

export default class PackageA {
  constructor() {}

  render() {
    console.log('PACKAGE A has jquery version: ', $.fn.jquery);
    $('#change-me-a').text(`Package A changed this text by jQuery version ${$.fn.jquery}`);
  }
}

// new PackageA().render();
