import $ from "jquery";
window.$ = require('jquery');

import PackageA from 'package-a';
import PackageB from 'package-b';

export default class MainApp {
  constructor() {}

  render() {
    console.log('MAIN APP has jquery version: ', $.fn.jquery);
    $('#change-me-main').text(`Main App changed this text by jQuery version ${$.fn.jquery}`);

    new PackageA().render();
    new PackageB().render();
  }
}

new MainApp().render();
